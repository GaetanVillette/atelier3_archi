package pokemon.user.MS_user_rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pokemon.user.MS_user_controller.UserService;
import pokemon.user.MS_user_model.Card;
import pokemon.user.MS_user_model.User;
import pokemon.user.MS_user_model.UserCards;

@RestController
@RequestMapping("/users")
public class UserRestCtrl {
	
    @Autowired
    UserService userService;
    
    public UserRestCtrl(){
    	
    }

    @GetMapping("/list")
    public List<User> getAllUsers()
    {
        return userService.getUsers();
    }

    @GetMapping("/{idUser}")
    public UserCards getUser(@PathVariable int idUser){
        return userService.getUser(idUser);
    }
    
    @GetMapping("/{idUser}/cards")
    public List<Card> getCardsByUserId(@PathVariable String idUser) {
    	return userService.getCardsByUserId(Integer.parseInt(idUser));
    }
    
    @GetMapping("/getUserByLogin/{login}")
    public User getUserByLogin(@PathVariable String login) {
    	return userService.getUserByLogin(login);
    }
    
    @GetMapping("/isLoginTaken/{login}")
    public boolean isLoginTaken(@PathVariable String login) {
    	return userService.isLoginTaken(login);
    }
    
    @PutMapping("/{idUser}/setBalance/{balance}")
    public void updateBalance(@PathVariable String idUser,@PathVariable String balance){
        userService.updateBalance(Integer.parseInt(idUser), Integer.parseInt(balance));
    }

    @PostMapping("/create")
    public void addUser(@RequestBody User user){
        userService.addUser(user);
    }

    @DeleteMapping("/delete/{idUser}")
    public boolean deleteUser(@RequestBody String idUser){
    	return userService.deleteUser(Integer.parseInt(idUser));
    }
}