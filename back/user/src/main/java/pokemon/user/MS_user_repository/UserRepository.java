package pokemon.user.MS_user_repository;

import org.springframework.data.repository.CrudRepository;

import pokemon.user.MS_user_model.User;

public interface UserRepository extends CrudRepository<User, Integer>{

}

