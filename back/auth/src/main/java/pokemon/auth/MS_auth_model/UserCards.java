package pokemon.auth.MS_auth_model;

import java.util.List;


public class UserCards {
	
	  public final User user;
	  public final List<Card> cards;
	  
	  public UserCards(User user, List<Card> cards) {
		    this.user = user;
		    this.cards = cards;
	  }

	public User getUser() {
		return user;
	}

	public List<Card> getCards() {
		return cards;
	}
	  
	  
}

