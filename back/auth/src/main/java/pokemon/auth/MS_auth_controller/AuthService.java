package pokemon.auth.MS_auth_controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import pokemon.auth.MS_auth_model.Card;
import pokemon.auth.MS_auth_model.User;
import pokemon.auth.MS_auth_model.UserCards;

@Service
public class AuthService {

	@Value("${api.user}")
	private String apiUser;
	@Value("${api.card}")
	private String apiCard;
	
	public Object register(User u) {
		Object result = -1;
		if(!StringUtils.isEmpty(u.getLogin()) && !StringUtils.isEmpty(u.getPassword()) && !StringUtils.isEmpty(u.getName())){
			RestTemplate restT = new RestTemplate();
	        String srcUrl = apiUser+"isLoginTaken/"+u.getLogin();
	        ResponseEntity<Boolean> resp = restT.getForEntity(srcUrl, Boolean.class);
	        Boolean isTaken = resp.getBody();
	        
			if(!isTaken){
				srcUrl = apiUser+"create";
				HttpEntity<User> request = new HttpEntity<>(u);
				restT.postForObject(srcUrl, request, User.class);

		        srcUrl = apiUser+"getUserByLogin/"+u.getLogin();
		        ResponseEntity<User> uResp = restT.getForEntity(srcUrl, User.class);
		        User newUser = uResp.getBody();
		        
		        srcUrl = apiCard+"cards/user/"+newUser.getId();
		        ResponseEntity<List<Card>> respE = restT.exchange(srcUrl, HttpMethod.GET, null,
		                    new ParameterizedTypeReference<List<Card>>() {} );
		        List<Card> cards = respE.getBody();
				UserCards userC = new UserCards(newUser,cards);
				return userC;
			}
		}
		return result;
	}
	
	public Object login(String login, String pwd) {
		Object result = -1;
		if(!login.isEmpty() && !pwd.isEmpty()){
	        String rsrcUrl = apiUser+"getUserByLogin/"+login;
	        RestTemplate restT = new RestTemplate();
	        ResponseEntity<User> uResp = restT.getForEntity(rsrcUrl, User.class);
	        User u = uResp.getBody();
			if(u != null) {
				if(u.getPassword().equals(pwd)) {
			        rsrcUrl = apiCard+"cards/user/"+u.getId();
			        ResponseEntity<List<Card>> respE = restT.exchange(rsrcUrl, HttpMethod.GET, null,
			                    new ParameterizedTypeReference<List<Card>>() {} );
			        List<Card> cards = respE.getBody();
					UserCards userC = new UserCards(u,cards);
					return userC;
				}
			}
		}
		return result;
	}
}
