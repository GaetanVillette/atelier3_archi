package pokemon.auth.MS_auth_rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pokemon.auth.MS_auth_controller.AuthService;
import pokemon.auth.MS_auth_model.User;

@RestController

public class AuthRestCtrl {
	
	@Autowired
	AuthService aService;

    @PostMapping("/register")
    public Object register(@RequestBody User u) {
    	return aService.register(u);
    }
    
    @GetMapping("/login/{login}/{password}")
    public Object login(@PathVariable String login, @PathVariable String password) {
    	return aService.login(login, password);
    }
    
}

