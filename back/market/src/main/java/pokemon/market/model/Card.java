package pokemon.market.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Card")
public class Card 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected int id;
	
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "cardDescriptionId", referencedColumnName = "id")
    protected CardDescription cardDescription;
    
    protected int ownerId;
    
    protected boolean forSale;
	
    
	public Card() {
	}

	public Card(CardDescription cardDescription, int ownerId, boolean forSale) {
		super();
		this.cardDescription = cardDescription;
		this.ownerId = ownerId;
		this.forSale = forSale;
	}
	
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(int userId) {
		this.ownerId = userId;
	}

	public void setCardDescription(CardDescription cardDescription) {
		this.cardDescription = cardDescription;
	}

	public CardDescription getCardDescription() {
		return cardDescription;
	} 
	
	public void setCardId(CardDescription cardDescription) {
		this.cardDescription = cardDescription;
	}
	
	public boolean isForSale() {
		return forSale;
	}
	
	public void setForSale(boolean forSale) {
		this.forSale = forSale;
	}	
}