package pokemon.market.model;

import java.util.ArrayList;
import java.util.List;


public class UserCards {

      public User user;
      public List<Card> cards;

      public UserCards(User user, List<Card> cards) {
            this.user = user;
            this.cards = cards;
      }
      public UserCards() {
        this.cards = new ArrayList<Card>();
    }

    public User getUser() {
        return user;
    }

    public List<Card> getCards() {
        return cards;
    }


}