package pokemon.market.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import pokemon.market.model.Card;
import pokemon.market.model.User;
import pokemon.market.model.UserCards;

@Service
public class MarketService {
	
	@Value("${api.user}")
	private String apiUser;
	@Value("${api.card}")
	private String apiCard;
	
	static final int REMOVED_FROM_SALE = -2;
	static final int ERROR = -1;

	public int buyCard(int idBuyer, int idCard) {
		int isTransacOk = -1;
	    final String buyerId = apiUser+idBuyer;
	    final String cardId = apiCard+"cards/"+idCard;
	    
	    RestTemplate restT = new RestTemplate();
	    ResponseEntity<UserCards> resp = restT.getForEntity(buyerId,UserCards.class);
	    UserCards buyerC = resp.getBody();
	    User buyer = buyerC.getUser();
	    RestTemplate restTC = new RestTemplate();
	    ResponseEntity<Card> respC = restTC.getForEntity(cardId, Card.class);
		Card boughtC = respC.getBody();
		
		if(boughtC != null && buyer != null) {
			int price = boughtC.getCardDescription().getPrice();
			if(buyer.getBalance() >= price && boughtC.isForSale()) {
				int sellerid = boughtC.getOwnerId();
			    final String sellerId = apiUser+sellerid;
			    ResponseEntity<UserCards> respSeller= restT.getForEntity(sellerId,UserCards.class);
			    UserCards sellerC = respSeller.getBody();
			    User seller = sellerC.getUser();
			    final String balanceSeller = apiUser+seller.getId()+"/setBalance/"+(seller.getBalance()+price);
			    RestTemplate restTB = new RestTemplate();
			    
			    HttpEntity<User> updateSeller = new HttpEntity<>(seller);
			    restTB.exchange(balanceSeller, HttpMethod.PUT, updateSeller, Boolean.class);
			    
			    final String balanceBuyer = apiUser+buyer.getId()+"/setBalance/"+(buyer.getBalance()-price);
			    HttpEntity<User> updateBuyer = new HttpEntity<>(buyer);
			    restTB.exchange(balanceBuyer, HttpMethod.PUT, updateBuyer, Boolean.class);

			    final String setInsale = apiCard+"cards/setForSale/"+idCard+"/false";
			    restTB.exchange(setInsale, HttpMethod.PUT, updateBuyer, Boolean.class);
			    
			    final String updateOwner = apiCard+"cards/updateOwner/"+idBuyer+"/"+boughtC.getId();
			    HttpEntity<User> changeOwner = new HttpEntity<>(buyer);
			    restTB.exchange(updateOwner, HttpMethod.PUT, changeOwner, Boolean.class);
			    
				isTransacOk = buyer.getBalance()-price;
			}
		}
		
		return isTransacOk;
	}
	
	public int sellCard(int idSeller, int idCard) {
		int sellStatus = ERROR;
	    final String sellerUri = apiUser+idSeller;
	    RestTemplate restTB = new RestTemplate();
	    ResponseEntity<UserCards> respSeller = restTB.getForEntity(sellerUri, UserCards.class);
	    UserCards sellerC = respSeller.getBody();
		User seller = sellerC.getUser();
		

	    final String cardId = apiCard+"cards/"+idCard;
	    RestTemplate restTC = new RestTemplate();
	    ResponseEntity<Card> respC = restTC.getForEntity(cardId, Card.class);
		Card sellingCard = respC.getBody();
		int ownerId = sellingCard.getOwnerId();
		
	    RestTemplate restTO = new RestTemplate();
	    final String ownerIdUri = apiUser+ownerId;
	    ResponseEntity<UserCards> respOwner = restTO.getForEntity(ownerIdUri,UserCards.class);
	    UserCards ownerC = respOwner.getBody();
	    User owner = ownerC.getUser();
	    
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		RestTemplate restTBal = new RestTemplate();
		
		if(seller.getId() == owner.getId()) {
			if(sellingCard.isForSale()) {
			    final String setInsale = apiCard+"cards/setForSale/"+idCard+"/false";
			    HttpEntity<User> updateBuyer = new HttpEntity<>(seller);
			    restTBal.exchange(setInsale, HttpMethod.PUT, updateBuyer, Boolean.class);
				sellStatus = REMOVED_FROM_SALE;
			}
			else {
			    final String setoffsale = apiCard+"cards/setForSale/"+idCard+"/true";
			    HttpEntity<User> requestUpdateBuyer = new HttpEntity<>(seller);
			    restTBal.exchange(setoffsale, HttpMethod.PUT, requestUpdateBuyer, Boolean.class);
				sellStatus = seller.getBalance();
			}
		}
		return sellStatus;
	}

	public List<Card> getMarketCards() {
		RestTemplate restT = new RestTemplate();
        String url = apiCard+"cards/market/list";
        ResponseEntity<List<Card>> responseEntity = restT.exchange(url, HttpMethod.GET, null,
                    new ParameterizedTypeReference<List<Card>>(){} );
        List<Card> cards = responseEntity.getBody();
		return cards;
	}
	
	
}