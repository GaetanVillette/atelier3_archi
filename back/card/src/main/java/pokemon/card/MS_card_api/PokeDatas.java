package pokemon.card.MS_card_api;

import java.util.List;

public class PokeDatas {
	
	private List<PokeData> results;
	
	public void setResults(List<PokeData> results) { this.results = results; }

	public List<PokeData> getResults() { return results; }

}
