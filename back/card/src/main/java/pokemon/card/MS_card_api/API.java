package pokemon.card.MS_card_api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import pokemon.card.MS_card_model.CardDescription;

public class API {
	
	private static API instance;
	private List<CardDescription> cartes;
	
	private static final int MIN_PRICE = 10;
	private static final int MAX_PRICE = 100;
	
	private API() {
		cartes = new ArrayList<>();
		try {	
			PokeDatas pokemons = (PokeDatas) mapData(PokeDatas.class, requestTo("https://pokeapi.co/api/v2/pokemon?limit=151"));

			for (PokeData pokemon : pokemons.getResults()) {
				cartes.add(new CardDescription(pokemon.getName(),
						"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/"
						+ pokemon.getUrl().split("https://pokeapi.co/api/v2/pokemon/")[1].split("/")[0] + ".png",
						MIN_PRICE + (int)(Math.random() * ((MAX_PRICE - MIN_PRICE) + 1))));
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	public CardDescription getRandomCard() {
		Collections.shuffle(cartes);
		return cartes.get(0);
	}
	
	public static API getInstance() {
		if (instance == null) {
			instance = new API();
		}
		return instance;
	}
	
	public static String requestTo(String apiUrl) throws IOException {
		StringBuilder content = new StringBuilder();
		URL url = new URL(apiUrl);
		HttpURLConnection httpRequest = (HttpURLConnection) url.openConnection();
		httpRequest.setRequestMethod("GET");
		httpRequest.addRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0");
		BufferedReader in = new BufferedReader(
		  new InputStreamReader(httpRequest.getInputStream()));
		String inputLine;
		while ((inputLine = in.readLine()) != null) {
		    content.append(inputLine);
		}
		in.close();
		
		return content.toString();
	}
	
	public static <T> Object mapData(Class<T> theClass, String data) throws JsonProcessingException, InstantiationException, IllegalAccessException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return objectMapper.readValue(data, theClass.newInstance().getClass());
	}
}

