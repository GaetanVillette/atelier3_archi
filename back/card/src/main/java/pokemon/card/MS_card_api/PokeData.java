package pokemon.card.MS_card_api;

public class PokeData {
	
	private String name;
	private String url;
	public String getName() { return name; }
	public String getUrl() { return url; }
	public void setName(String name) { this.name = name; }
	public void setUrl(String url) { this.url = url; }

}
