package pokemon.card.MS_card_repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import pokemon.card.MS_card_model.CardDescription;

public interface CardDescriptionRepo extends CrudRepository<CardDescription, Integer>{
	
	public List<CardDescription> findAll();
}

