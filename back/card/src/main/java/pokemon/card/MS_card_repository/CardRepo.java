package pokemon.card.MS_card_repository;

import org.springframework.data.repository.CrudRepository;

import pokemon.card.MS_card_model.Card;

public interface CardRepo extends CrudRepository<Card, Integer> {
	
}

