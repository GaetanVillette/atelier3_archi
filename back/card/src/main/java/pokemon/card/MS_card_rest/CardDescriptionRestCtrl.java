package pokemon.card.MS_card_rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pokemon.card.MS_card_controller.CardDescriptionService;
import pokemon.card.MS_card_model.CardDescription;

@RestController
@RequestMapping("/cardDesc")
public class CardDescriptionRestCtrl {

    @Autowired
    CardDescriptionService cardDescService;
    
    CardDescriptionRestCtrl(){

    }

    @GetMapping("/list")
    public List<CardDescription> getAllCardsDes(){
        return cardDescService.getAllCardsDes();
    }
    
    @GetMapping("/random")
    public CardDescription getRandomCardDes(){
        return cardDescService.getRandomCardDes();
    }

    @GetMapping("/{id}")
    public CardDescription getCardDesById(@PathVariable String idCardDes){
        return cardDescService.getCardDesById(Integer.parseInt(idCardDes));
    }

    @PostMapping("/create")
    public boolean addCard(@RequestBody CardDescription cardDes){
        cardDescService.addCard(cardDes);
        return true;
    }

    @DeleteMapping("/delete/{id}")
    public boolean deleteCardDesById(@RequestBody String id){
        cardDescService.deleteCardDesById(Integer.parseInt(id));
        return true;
    }
}

