package pokemon.card.MS_card_rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pokemon.card.MS_card_controller.CardService;
import pokemon.card.MS_card_model.Card;

@RestController
@RequestMapping("/cards")
public class CardRestCtrl {

    @Autowired
    CardService cardService;

    public CardRestCtrl(){

    }

    @GetMapping("/list")
    public List<Card> getCardsList(){
        return cardService.getCardsList();
    }
    
    @GetMapping("/market/list")
    public List<Card> getMarketCardsList(){
        return cardService.getMarketCardsList();
    }
    
    @PostMapping("/create")
    public Card addCard(@RequestBody Card card){
        return cardService.addCard(card);
    }

    @GetMapping("/{id}")
    public Card getCardById(@PathVariable String id){
        return cardService.getCardById(Integer.parseInt(id));
    }
    
    @GetMapping("/user/{userId}")
    public List<Card> getCardsByUserId(@PathVariable String userId){
        return cardService.getCardsByUserId(Integer.parseInt(userId));
    }
    
    @PutMapping("/setForSale/{id}/{bool}")
    public boolean setForSale(@PathVariable int id, @PathVariable Boolean bool){
        cardService.setForSale(id, bool);
        return true;
    }

    @PutMapping("/updateOwner/{userId}/{cardId}")
    public boolean updateOwner(@PathVariable int userId, @PathVariable int cardId){
        cardService.updateOwner(cardId, userId);
        return true;
    }
    
    @DeleteMapping("/delete/{id}")
    public boolean deleteCardById(@PathVariable String id){
        cardService.deleteCard(Integer.parseInt(id));
        return true;
    }
}