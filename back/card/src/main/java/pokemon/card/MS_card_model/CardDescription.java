package pokemon.card.MS_card_model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cardDescription")
public class CardDescription {

	@Id
	@GeneratedValue
	private int id;
	
	private String name;
	private String image;
	private int price;

	public CardDescription() {}
	
	public CardDescription(int id) {
		super();
		this.id = id;
	}
	
	public CardDescription(String name, String image) {
		super();
		this.name = name;
		this.image = image;
	}
	
	public CardDescription(String name, String image, int price) {
		super();
		this.name = name;
		this.image = image;
		this.price = price;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
}
