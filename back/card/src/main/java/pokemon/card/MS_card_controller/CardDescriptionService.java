package pokemon.card.MS_card_controller;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import pokemon.card.MS_card_api.API;
import pokemon.card.MS_card_model.CardDescription;
import pokemon.card.MS_card_repository.CardDescriptionRepo;

@Service
public class CardDescriptionService
{
	@Autowired
	private CardDescriptionRepo cardDescriptionRepo;
	
	public CardDescriptionService() {
	}
	
	public List<CardDescription> getAllCardsDes() {
		return cardDescriptionRepo.findAll();
	}
	
	public CardDescription getRandomCardDes() {
		CardDescription cd = API.getInstance().getRandomCard();
		addCard(cd);
		return cd;
	}
	
	public CardDescription getCardDesById(int id) {
		Optional<CardDescription> cardDesOpt = cardDescriptionRepo.findById(id);
		return cardDesOpt.isPresent() ? cardDesOpt.get() : null;
	}

	public void addCard(CardDescription cardDes) {
		cardDescriptionRepo.save(cardDes);
	}
	
	public void deleteCardDesById(int id) {
		cardDescriptionRepo.delete(this.getCardDesById(id));
	}
}