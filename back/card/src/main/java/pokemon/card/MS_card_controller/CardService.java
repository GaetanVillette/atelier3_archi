package pokemon.card.MS_card_controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pokemon.card.MS_card_model.Card;
import pokemon.card.MS_card_repository.CardRepo;

@Service
public class CardService {
	
	@Autowired
	private CardRepo cardRepo;
	
	public List<Card> getCardsList() {
		return (List<Card>)cardRepo.findAll();
	}
	
	public List<Card> getMarketCardsList() {
		List<Card> cardsMarket = new ArrayList<>();
		
		for (Card card : this.getCardsList()) {
			if (card.isForSale()) {
				cardsMarket.add(card);
			}
		}
		
		return cardsMarket;
	}
	
	public Card addCard(Card card) {
		return cardRepo.save(card);
	}
	
	public Card getCardById(int id) {
		Optional<Card> cardOpt = cardRepo.findById(id);
		return (cardOpt.isPresent()) ? cardOpt.get() : null;
	}
	
	public List<Card> getCardsByUserId(int userId) {
		List<Card> cardsUserId = new ArrayList<>();
		
		for (Card card : this.getCardsList()) {
			if (card.getOwnerId() == userId) {
				cardsUserId.add(card);
			}
		}
		
		return cardsUserId;
	}
	
	public void setForSale(int id, boolean state) {
		Card card = getCardById(id);
		card.setForSale(state);
		cardRepo.save(card);
	}

	public void updateOwner(int cardId, int userId) {
		Card card = getCardById(cardId);
		card.setOwnerId(userId);
		cardRepo.save(card);
    }
	
	public void deleteCard(int id) {
		cardRepo.delete(this.getCardById(id));
	}
}