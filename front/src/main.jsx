import { ThemeProvider } from '@material-ui/core'
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'

import Routeur from './components/Routeur'
import customTheme from './theme';
import { persistor, store} from './redux/store';

ReactDOM.render(
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      <ThemeProvider theme={customTheme()}>
        <Routeur />
      </ThemeProvider>
    </PersistGate>
  </Provider>,
  document.getElementById('root')
)
