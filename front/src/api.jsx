import { DUREE_MESSAGE } from './utils';
import { hideAction, showAction } from './redux/message';
import { loginAction } from "./redux/user";
import { getAction } from './redux/marche';

const erreurInscriptionTexte = 'Erreur dans les informations d\'inscription, login déjà utilisé';
const erreurLoginTexte = 'Erreur dans les informations de connexion';
const erreurMarcheTexte = 'Erreur dans la récupération des cartes du marché';
const erreurTransactionTexte = 'Erreur dans la transaction de la carte';

const erreur = (dispatch, texte) => {
    dispatch(showAction({
        texte: texte,
        type: 'error',
    }));
    setTimeout(() => dispatch(hideAction()), DUREE_MESSAGE);
}

const erreurRequete = (res) => {
    if (res.status >= 200 && res.status < 300) {
        return res.json();
    }
    return 'Error';
}

export const inscription = (dispatch, param) => {
    fetch('api/auth/register', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(param),
    })
    .then(erreurRequete)
    .catch(() => erreur(dispatch, erreurInscriptionTexte))
    .then(data => {
        if (data !== -1) {
            dispatch(showAction({
                texte: 'Vous vous êtes inscris, connectez vous maintenant',
                type: 'success',
            }));
            setTimeout(() => dispatch(hideAction()), DUREE_MESSAGE);
        } else {
            erreur(dispatch, erreurInscriptionTexte);
        }
    })
    .catch(() => erreur(dispatch, erreurInscriptionTexte))
}

export const login = (dispatch, param, withMessage) => {
    fetch('api/auth/login/' + param.login + '/' + param.password, {
        method: 'GET',
    })
    .then(erreurRequete)
    .catch(() => withMessage && erreur(dispatch, erreurLoginTexte))
    .then(data => {
        dispatch(loginAction(data));
        if (withMessage) {
            if (data !== -1) {
                dispatch(showAction({
                    texte: 'Vous vous êtes connecté',
                    type: 'success',
                }));
                setTimeout(() => dispatch(hideAction()), DUREE_MESSAGE);
            } else {
                erreur(dispatch, erreurLoginTexte);
            }
        }
    })
    .catch(() => withMessage && erreur(dispatch, erreurLoginTexte))
}

export const marche = (dispatch) => {
    fetch('api/market/market/', {
        method: 'GET',
    })
    .then(erreurRequete)
    .catch(() => erreur(dispatch, erreurMarcheTexte))
    .then(data => {
        if (data !== -1) {
            dispatch(getAction(data));
        } else {
            erreur(dispatch, erreurMarcheTexte);
        }
    })
    .catch(() => erreur(dispatch, erreurMarcheTexte))
}

export const transaction = (type, dispatch, param) => {
    fetch('api/market/' + type + '/' + param.userId + '/' + param.cardId, {
        method: 'PUT',
    })
    .then(erreurRequete)
    .catch(() => erreur(dispatch, erreurTransactionTexte))
    .then((res) => {
        if (res != -1 && res != -2) {
            login(dispatch, ({ login: param.login, password: param.password }));
            marche(dispatch);
            dispatch(showAction({
                texte: type == 'sell' ? 'Vous avez mis en vente votre carte' : 'Vous avez acheté la carte',
                type: 'success',
            }));
            setTimeout(() => dispatch(hideAction()), DUREE_MESSAGE);
        } else {
            erreur(dispatch, erreurTransactionTexte)
        }
    })
    .catch(() => erreur(dispatch, erreurTransactionTexte))
}