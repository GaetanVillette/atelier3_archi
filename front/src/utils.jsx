export const capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

export const DUREE_MESSAGE = 2000;

export const cutArrayByNbElements = (array, nb) => {
    const newArray = [];
    for (let i = 0; i < Math.ceil(array.length / nb); i++) {
        const line = [];
        for (let j = i * nb; j < (i+1) * nb; j++) {
            if (array[j]) {
                line.push(array[j]);
            }
        }
        newArray.push(line);
    }
    return newArray;
}