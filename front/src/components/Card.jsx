import { Button, Card, CardContent, Typography } from '@material-ui/core';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getUserState } from '../redux/user';

import Div from './Div';
import { capitalizeFirstLetter, cutArrayByNbElements } from '../utils';
import { login as apiLogin, transaction } from '../api';

export default () => {
    const dispatch = useDispatch();
    const { cards, id: userId, login, password } = useSelector(getUserState);

    useEffect(() => apiLogin(dispatch, ({ login, password })), []);

    const cuttedCards = cutArrayByNbElements(cards, 6);

    return (
        <Div p="20px" center middle>
            <Div v>
                {cuttedCards && cuttedCards.map((line, indexLine) => (
                    <Div center middle key={indexLine} mb={indexLine !== cuttedCards.length - 1 && '10px'}>
                        {line && line.map(({ id: cardId, forSale, cardDescription: { name, image, price } }, index) => (
                            <Div key={cardId} mr={index !== line.length - 1 && '10px'}>
                                <Card>
                                    <CardContent>
                                        <Typography variant="h6" color="secondary">{capitalizeFirstLetter(name)}</Typography>
                                        <img src={image} width="200" height="200" />
                                    </CardContent>
                                    <Div p="10px">
                                        <Div w="50%">
                                            <Typography variant="h6" color="primary">
                                                {`${price}💰`}
                                            </Typography>
                                        </Div>
                                        <Div w="50%" right>
                                            <Button disabled={forSale} onClick={() => transaction('sell', dispatch, ({ userId, cardId, login, password }))}>
                                                Vendre
                                            </Button>
                                        </Div>
                                    </Div>
                                </Card>
                            </Div>
                        ))}
                    </Div>
                ))}
            </Div>
            {cuttedCards.length === 0 && <Typography variant="h6" color="secondary">Vous n'avez aucune carte</Typography>}
        </Div>
    )
}