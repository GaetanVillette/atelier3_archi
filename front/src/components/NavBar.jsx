import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppBar, Button, Dialog, Tab, Tabs, TextField, Toolbar, Typography, withStyles } from '@material-ui/core';
import { Redirect } from 'react-router-dom';
import Slide from '@material-ui/core/Slide';

import pokeball from '../images/pokeball.png';
import Div from './Div';
import Inscription from './Inscription';
import { login as apiLogin } from '../api';
import { disconnectAction, getUserState } from '../redux/user';
import { capitalizeFirstLetter, DUREE_MESSAGE } from '../utils';
import { hideAction, showAction } from '../redux/message';

const StyledTab = withStyles({
    root: {
        fontSize: '1rem',
        minHeight: '64px',
    },
})(Tab);

const StyledToolbar = withStyles({
    root: {
        minHeight: '64px',
        padding: '0px',
    },
})(Toolbar);

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const NavBar = () => {
    const dispatch = useDispatch();
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const [redirection, setRedirection] = useState('');
    const [value, setValue] = useState(0);
    const [open, setOpen] = useState(false);
    const openPopUp = () => setOpen(true);
    const closePopUp = () => setOpen(false);

    useEffect(() => {
        if (window.location.pathname === '/marche') {
            handleChange(null, 1);
        }
    }, []);
    
    const handleChange = (e, newValue) => setValue(newValue);

    useEffect(() => setRedirection(''), [redirection]);

    const userState = useSelector(getUserState);

    const loggedIn = userState && userState.id;

    const onDisconnect = () => {
        dispatch(disconnectAction());
        dispatch(showAction({
            texte: 'Vous vous êtes déconnecté',
            type: 'success',
        }));
        setTimeout(() => dispatch(hideAction()), DUREE_MESSAGE);
    }

    return (
        <AppBar color="default" position="static">
            <Div>
                {redirection && <Redirect to={redirection} />}
                <Div middle pl="10px" w="calc(12% - 10px)">
                    <img src={pokeball} height="50" width="50" />
                    <Div w="10px" />
                    <Typography variant="h4" color="primary">
                        Pokarchi
                    </Typography>
                </Div>
                <Div w="30%">
                    {loggedIn ? (
                        <StyledToolbar variant="dense">
                            <Tabs
                                indicatorColor="primary"
                                textColor="primary"
                                value={value}
                                onChange={handleChange}
                            >
                                <StyledTab
                                    label="Cartes"
                                    onClick={() => setRedirection('cartes')}
                                />
                                <StyledTab
                                    label="Marché"
                                    onClick={() => setRedirection('marche')}
                                />
                            </Tabs>
                        </StyledToolbar>
                    ) : (
                        <StyledToolbar variant="dense" />
                    )}
                </Div>
                <Div middle right pr="10px" w="calc(58% - 10px)">
                {loggedIn ? (
                    <>
                        <Typography variant="h6" color="secondary">
                            {`${capitalizeFirstLetter(userState.login)}: ${userState.balance}💰`}
                        </Typography>
                        <Div w="20px" />
                        <Button
                            onClick={onDisconnect}
                        >
                            Déconnexion
                        </Button>
                    </>
                ) : (
                    <>
                        <TextField
                            label="Login"
                            onChange={(e) => setLogin(e.target.value)}
                            required
                        />
                        <Div w="10px" />
                        <TextField
                            label="Mot de passe"
                            type="password"
                            onChange={(e) => setPassword(e.target.value)}
                            required
                        />
                        <Div w="10px" />
                        <Button
                            onClick={() => apiLogin(dispatch, { login, password }, true)}
                        >
                            Connexion
                        </Button>
                        <Div w="10px" />
                        <Button
                            onClick={openPopUp}
                            color="secondary"
                        >
                            Inscription
                        </Button>
                        <Dialog open={open} onClose={closePopUp} TransitionComponent={Transition}>
                            <Inscription close={closePopUp} />
                        </Dialog>
                    </>
                )}
                </Div>
            </Div>
        </AppBar>
    );
};

export default NavBar;