import React, { useState } from 'react';
import { Button, TextField, Typography } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { useDispatch } from 'react-redux';


import Div from './Div';
import { inscription } from '../api';

export default ({ close }) => {
    const dispatch = useDispatch();
    const [login, setLogin] = useState('');
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');

    return (
        <Div v>
            <Div bc="#e6e2e1" h="50px" p="10px">
                <Div ml="10px" center middle>
                    <Typography variant="h5" color="primary">
                        Inscription
                    </Typography>
                </Div>
                <Div w="300px" />
                <Div right>
                    <IconButton onClick={close}>
                        <CloseIcon />
                    </IconButton>
                </Div>
            </Div>
            <Div p="20px" center v>
                    <TextField
                        label="Login"
                        onChange={(e) => setLogin(e.target.value)}
                        required
                    />
                    <Div h="10px" />
                    <TextField
                        label="Nom"
                        onChange={(e) => setName(e.target.value)}
                        required
                    />
                    <Div h="10px" />
                    <TextField
                        label="Mot de passe"
                        type="password"
                        onChange={(e) => setPassword(e.target.value)}
                        required
                    />
            </Div>
            <Div center p="20px" pb="10px" pt="0px">
                <Button
                    onClick={() => {
                        inscription(dispatch, { login, name, password });
                        close();
                    }}
                >
                    Valider
                </Button>
            </Div>
        </Div>
    )
}