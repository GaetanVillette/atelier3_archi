/* eslint-disable react/require-default-props */
import React from 'react';
import styled from 'styled-components';

const StyledDiv = styled.div`
    display: flex;
    flex-direction: ${({ v }) => (v ? 'column' : 'row')};
    ${({ b }) => b && `bottom: ${b};`}
    ${({ t }) => t && `top: ${t};`}
    ${({ bc }) => bc && `background-color: ${bc};`}
    ${({ w }) => w && `width: ${w};`}
    ${({ c }) => c && `color: ${c};`}
    ${({ h }) => h && `height: ${h};`}
    ${({ fixed }) => fixed && 'position: fixed;'}
    ${({ center }) => center && 'justify-content: center;'}
    ${({ right }) => right && 'justify-content: flex-end;'}
    ${({ middle }) => middle && 'align-items: center;'}
    ${({ m }) => m && `padding: ${m};`}
    ${({ mb }) => mb && `margin-bottom: ${mb};`}
    ${({ ml }) => ml && `margin-left: ${ml};`}
    ${({ mt }) => mt && `margin-top: ${mt};`}
    ${({ mr }) => mr && `margin-right: ${mr};`}
    ${({ p }) => p && `padding: ${p};`}
    ${({ pb }) => pb && `padding-bottom: ${pb};`}
    ${({ pl }) => pl && `padding-left: ${pl};`}
    ${({ pt }) => pt && `padding-top: ${pt};`}
    ${({ pr }) => pr && `padding-right: ${pr};`}
    ${({ image }) => image && `background-image: url(${image});`}
    ${({ bold }) => bold && `font-weight: bold;`}
`;

export default ({
    b,
    bc,
    bold,
    c,
    center,
    fixed,
    h,
    image,
    middle,
    m,
    mb,
    ml,
    mt,
    mr,
    p,
    pb,
    pl,
    pt,
    pr,
    right,
    t,
    w,
    v,
    children,
}) => (
    <StyledDiv
        b={b}
        bc={bc}
        bold={bold}
        c={c}
        center={center}
        fixed={fixed}
        h={h}
        image={image}
        middle={middle}
        m={m}
        mb={mb}
        ml={ml}
        mt={mt}
        mr={mr}
        p={p}
        pb={pb}
        pl={pl}
        pt={pt}
        pr={pr}
        right={right}
        t={t}
        w={w}
        v={v}
    >
        {children}
    </StyledDiv>
);