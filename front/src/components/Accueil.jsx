import React from 'react';
import { Typography } from '@material-ui/core';

import Div from './Div';

const texte = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur malesuada erat at orci iaculis imperdiet. Nunc iaculis leo mi, vel interdum mi ultricies ut. Nullam dapibus erat vel elit tempus, id ultricies justo lobortis. Etiam ac semper sapien. Curabitur tristique, dolor sed lobortis gravida, dolor quam condimentum lectus, non tincidunt lectus nunc ut metus. Mauris faucibus malesuada pellentesque. Etiam lobortis dui ut risus euismod sollicitudin.'
const texte2 = 'Vivamus augue ante, tristique nec fermentum et, ultrices sed quam. Fusce fringilla elit at aliquet ullamcorper. Phasellus scelerisque felis ac ultrices porttitor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla vel orci urna. In vel elit semper nibh vulputate luctus sit amet nec dolor. In ut euismod nisi. Proin sodales vel leo et convallis. Nulla sed ex magna. Vestibulum euismod, tellus quis vestibulum auctor, magna tortor cursus lectus, et volutpat sapien ex quis enim.'
const texte3 = 'Pellentesque vehicula diam sit amet purus convallis, ut tincidunt ex blandit. Fusce et augue ac eros vulputate posuere sit amet molestie eros. Donec et metus eget ligula dictum venenatis. Maecenas viverra finibus lorem, et faucibus est mattis eget. Aliquam sodales lacus accumsan libero volutpat, at tristique magna tempus. Duis ac tellus vestibulum, posuere augue non, facilisis ligula. Curabitur congue semper massa, ut facilisis urna auctor sit amet. Duis laoreet dapibus enim et feugiat. Duis pretium maximus mattis. Proin in orci non massa semper sagittis. Pellentesque non quam blandit nulla mattis bibendum et et ante. Nunc porttitor quam et fermentum luctus. Ut eleifend lacinia venenatis. Sed ut diam tempor ante pellentesque ornare ut quis mi.';
const texte4 = 'Suspendisse sit amet nibh ut libero sodales malesuada non ac lectus. Vivamus tellus eros, dignissim eu elementum at, tincidunt eu nisi. Quisque sed lacus commodo, suscipit mi ac, pharetra dui. Integer bibendum eros eget blandit maximus. Suspendisse dictum risus vel mi egestas, eget tincidunt odio finibus. Proin posuere ipsum at nunc hendrerit commodo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras interdum eget leo ac maximus. Vivamus turpis lorem, tempus at mi at, dignissim tempus sem. Nullam dictum sapien id commodo blandit. Suspendisse porttitor vel nisl id interdum. Duis fermentum commodo ante vel egestas.';
const texte5 = 'Praesent ac ullamcorper ipsum. Fusce sit amet felis odio. Proin ultricies pellentesque risus eget euismod. Cras mattis sem in odio interdum pharetra. Integer eu blandit ante, vel auctor sapien. Quisque tincidunt lobortis velit, et euismod urna sodales a. Suspendisse potenti. Sed ultrices finibus interdum. Vestibulum a turpis vel libero euismod lobortis a malesuada nulla. Aliquam venenatis erat nec purus convallis, vel varius mi ultricies.';

export default () => {


    return (
        <Div p="40px" pt="20px" center middle v>
            <Typography variant="h5" color="primary">Bienvenue sur Pokarchi</Typography>
            <Div h="20px" />
            <Typography variant="h6" color="secondary">{texte}</Typography>
            <Div h="10px" />
            <Typography variant="h6" color="secondary">{texte2}</Typography>
            <Div h="10px" />
            <Typography variant="h6" color="secondary">{texte3}</Typography>
            <Div h="10px" />
            <Typography variant="h6" color="secondary">{texte4}</Typography>
            <Div h="10px" />
            <Typography variant="h6" color="secondary">{texte5}</Typography>
        </Div>
    )
}