import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Card, CardContent, Typography } from '@material-ui/core';

import Div from './Div';
import { capitalizeFirstLetter, cutArrayByNbElements } from '../utils';
import { marche, transaction } from '../api';
import { getMarcheState } from '../redux/marche';
import { getUserState } from '../redux/user';

export default () => {
    const dispatch = useDispatch();
    const { id: userId, login, password } = useSelector(getUserState);
    const cards = useSelector(getMarcheState);
    useEffect(() => marche(dispatch), []);

    const cuttedCards = cutArrayByNbElements(cards, 6);

    return (
        <Div p="20px" center middle>
            <Div v>
                {cuttedCards && cuttedCards.map((line, indexLine) => (
                    <Div center middle key={indexLine} mb={indexLine !== cuttedCards.length - 1 && '10px'}>
                        {line && line.map(({ id: cardId, ownerId, cardDescription: { name, image, price } }, index) => (
                            <Div key={cardId} mr={index !== line.length - 1 && '10px'}>
                                <Card>
                                    <CardContent>
                                        <Typography variant="h6" color="secondary">{capitalizeFirstLetter(name)}</Typography>
                                        <img src={image} width="200" height="200" />
                                    </CardContent>
                                    <Div p="10px">
                                        <Div w="50%">
                                            <Typography variant="h6" color="primary">
                                                {`${price}💰`}
                                            </Typography>
                                        </Div>
                                        <Div w="50%" right>
                                            <Button disabled={userId === ownerId} onClick={() => transaction('buy', dispatch, ({ userId, cardId, login, password }))}>
                                                Acheter
                                            </Button>
                                        </Div>
                                    </Div>
                                </Card>
                            </Div>
                        ))}
                    </Div>
                ))}
            </Div>
            {cuttedCards.length === 0 && <Typography variant="h6" color="secondary">Le marché est vide</Typography>}
        </Div>
    )
}