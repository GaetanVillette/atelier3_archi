import React, { useEffect } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { withStyles } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { useDispatch, useSelector } from 'react-redux';
import { getMessageState } from '../redux/message';

import Card from './Card';
import Div from './Div';
import NavBar from './NavBar';
import Marche from './Marche';
import { getUserState } from '../redux/user';
import { login as apiLogin } from '../api';
import Accueil from './Accueil';

const StyledAlert = withStyles({
    root: {
        width: '100%',
    },
})(Alert);

export default () => {
    const dispatch = useDispatch();
    const { texte, type } = useSelector(getMessageState);
    const { login, password } = useSelector(getUserState);

    useEffect(() => apiLogin(dispatch, ({ login, password })), []);

    const loggedIn = login && password;

    return (
        <BrowserRouter>
            <NavBar />
            <Div middle mt="5px" h="40px">
                {texte && type &&
                    <StyledAlert severity={type}>
                        {texte}
                    </StyledAlert>
                } 
            </Div>
            <Switch>
                {loggedIn &&
                    <>
                        <Route path="/cartes" component={Card} />
                        <Route path="/marche" component={Marche} />
                    </>
                }
                <Route path="/" component={Accueil} />
            </Switch>
            
        </BrowserRouter>
    );
}