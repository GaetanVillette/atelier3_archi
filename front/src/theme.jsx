import { createTheme } from '@material-ui/core';

export default () => createTheme({
    palette: {
        primary: {
            light: '#0bbf74',
            main: '#088c69',
            dark: '#06694e',
            contrastText: '#fff',
        },
        secondary: {
            light: '#bfb30b',
            main: '#8c8308',
            dark: '#695a06',
            contrastText: '#fff',
        },
    },
    props: {
        MuiAlert: {
            variant: 'filled',
        },
        MuiButton: {
            color: 'primary',
            size: 'small',
            variant: 'contained',
        },
        MuiTextField: {
            size: 'small',
            variant: 'outlined',
        },
    },
});