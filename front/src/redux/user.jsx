const DISCONNECT = 'DISCONNECT';
const LOGIN = 'LOGIN';

export const loginAction = ({ user, cards }) => ({
    type: LOGIN,
    payload: { cards, ...user },
});

export const disconnectAction = () => ({
    type: DISCONNECT,
})

export const getUserState = (state) => state.user;

export default (state = {}, action) => {
    switch (action.type) {
        case DISCONNECT:
            return {};
        case LOGIN:
            return action.payload;
        default:
            return state;
    }
};