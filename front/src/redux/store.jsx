import { combineReducers, createStore } from 'redux';
import { devToolsEnhancer } from 'redux-devtools-extension';
import { createTransform, persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import storageSession from 'redux-persist/lib/storage/session';
import { parse, stringify } from 'flatted';

import marcheReducer from './marche';
import messageReducer from './message';
import userReducer from './user';

const transformCircular = createTransform(
    (inboundState) => stringify(inboundState),
    (outboundState) => parse(outboundState),
);

const persistConfig = {
    key: 'root',
    storage: process.env.NODE_ENV === 'production' ? storage : storageSession,
    transforms: [transformCircular]
};

const rootReducer = combineReducers({
    marche: marcheReducer,
    message: messageReducer,
    user: userReducer,
});

const store = createStore(
    persistReducer(persistConfig, rootReducer),
    devToolsEnhancer({})
);

const persistor = persistStore(store);

export { persistor, store }