export const SHOW = 'SHOW';
export const HIDE = 'HIDE';

export const showAction = (msg) => ({
    type: SHOW,
    payload: msg,
});

export const hideAction = () => ({
    type: HIDE,
});

export const getMessageState = (state) => state.message;

export default (state = {}, action) => {
    switch (action.type) {
        case SHOW:
            return action.payload;
        case HIDE:
            return {};
        default:
            return state;
    }
};