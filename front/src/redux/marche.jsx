export const GET = 'GET';

export const getAction = (marche) => ({
    type: GET,
    payload: marche,
});

export const getMarcheState = (state) => state.marche;

export default (state = [], action) => {
    switch (action.type) {
        case GET:
            return action.payload;
        default:
            return state;
    }
};