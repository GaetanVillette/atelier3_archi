# Atelier3_Archi

## Mise en place de l'application

### Prérequis 
- Avoir téléchargé docker sinon : `sudo apt install docker`
- Avoir téléchargé docker-compose sinon : `sudo apt install docker-compose`
- Avoir libérer le port 80 sinon: `sudo service apache2 stop`

### Installer l'application
- `git clone https://gitlab.com/GaetanVillette/atelier3_archi.git`
- `cd atelier3_archi`
- `sudo docker-compose up --build`

### Vidéo de présentation
https://youtu.be/v92ZEyHUHPY

### Schéma de l'architecture micro-service mise en place
![](Archi_atelier_3.png)
